
/*
 * (c) Copyright 2020 Brite:Bill Ltd.
 *
 * 7 Grand Canal Street Lower, Dublin 2, Ireland
 * info@britebill.com
 * +353 1 661 9426
 */
package com.agiletestingalliance;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

/**
 * @author <a href="mailto:razvanm@amdocs.com">Razvan Mihai</a>
 */
public class UsefulnessTest {

    @Test
    public void testFunction() {
        Usefulness usefulness = new Usefulness();

        usefulness.functionWF();
    }

    @Test
    public void testDesc() {
        Usefulness usefulness = new Usefulness();

        assertTrue(usefulness.desc().startsWith("DevOps"));
    }
}
