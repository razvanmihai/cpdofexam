
/*
 * (c) Copyright 2020 Brite:Bill Ltd.
 *
 * 7 Grand Canal Street Lower, Dublin 2, Ireland
 * info@britebill.com
 * +353 1 661 9426
 */
package com.agiletestingalliance;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * @author <a href="mailto:razvanm@amdocs.com">Razvan Mihai</a>
 */
public class MinMaxTest {
    @Test
    public void testFunc() {
        MinMax minmax = new MinMax();

        assertEquals(2, minmax.function(1, 2));
        assertEquals(2, minmax.function(2, 1));
    }
}
