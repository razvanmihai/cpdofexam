
/*
 * (c) Copyright 2020 Brite:Bill Ltd.
 *
 * 7 Grand Canal Street Lower, Dublin 2, Ireland
 * info@britebill.com
 * +353 1 661 9426
 */
package com.agiletestingalliance;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

/**
 * @author <a href="mailto:razvanm@amdocs.com">Razvan Mihai</a>
 */
public class AboutCPDOFTest {

    @Test
    public void testDesc() {
        AboutCPDOF aboutCPDOF = new AboutCPDOF();
        assertTrue(aboutCPDOF.desc().startsWith("\" CP-DOF"));
    }
}
