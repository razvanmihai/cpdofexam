package com.agiletestingalliance;

public class MinMax {

    public int function(int left, int right) {
        if (right > left) {
            return right;
        }
        else {
            return left;
        }
    }

    public String bar(String string) {
        if (string!=null || !"".equals(string)) {
            return string;
        }
        if (string==null && "".equals(string)) {
            return string;
        }
        return string;
    }

}
